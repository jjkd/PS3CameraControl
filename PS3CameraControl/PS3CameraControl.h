/* 
   PS3CameraControl.h
   Definitions and such
*/

#define LANC_ENABLE true

//Set this to 1 for John's Arduino Mega
#define JOHN_MEGA true

#if JOHN_MEGA
#define MANUAL_FOCUS_CONTROLS false
#else
#define MANUAL_FOCUS_CONTROLS true
#endif

//Slowest possible pan and tilt speeds on your setup, you may have to tweak these
#define TILT_UP_CRAWL 255
#define TILT_DOWN_CRAWL 155
#define PAN_LEFT_CRAWL 75
#define PAN_RIGHT_CRAWL 80
#define SPEED_CRAWL 45
#define LEFT_SPEED_CRAWL 70


#define BAUD_RATE 115200
#define MAX_SPEED 255
#define MIN_SPEED 50
#define SPEED_INC 10
#define UP_DOWN_DISPARITY 30
#define LANC_NUM_CMD 6
#define PWM_SPEED (TCCR1B & 0b11111000) | 0x05
#define PWM_FREQ TCCR2B & 0b11111000 | 0x07
#define LANC_STAT_PAUSE 0x14
#define LANC_STAT_PLAY 0x06
#define LANC_STAT_REC 0x04
#define LED_BLINK_DELAY 200
#define SEARCH_GAP_DELAY 25
#define LANC_GAP_LIMIT 150
#define LANC_START_BIT_DELAY 70
#define LANC_BIT_DELAY 94
#define LANC_BYTE 8
#define LANC_READ_BIT_DELAY 104
#define BYTE_MASK 0xFF
#define LANC_NEXT_BIT_DELAY 5
#define BLUETOTH_ADDR_LEN 6
#define ZOOM_DEAD_SPACE 99
#define ZOOM_SPEED_NUM_OPS 17
#define SLOW_PAN_LEFT 115
#define FAST_NEGATIVE 55
#define SLOW_PAN_RIGHT 140
#define FAST_POSITIVE 200
#define JOSTICK_MIDDLE 127
#define SLOW_TILT_UP 120
#define SLOW_POSITIVE 135
#define SLOW_PAN_R_MAP 50
#define FAST_PAN_R_MAP 70
#define SLOW_PAN_L_MAP 60
#define FAST_PAN_L_MAP 80
#define LANC_NORMAL_CMD 0x18
#define LANC_SPECIAL_CMD 0x28
#define LANC_DONE_BYTES 2
#define LANC_REPORT 4
#define L2R2_MIN 7

