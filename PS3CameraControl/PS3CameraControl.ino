/*

 LANC Control - v1.0
 (C) Ariel Rocholl - 2008, Madrid Spain
 Feel free to share this source code, but include explicit mention to the author.
 Licensed under creative commons - see http://creativecommons.org/licenses/by-sa/3.0/

 */
#include <PS3BT.h>
#include <usbhub.h>
#include "PS3CameraControl.h"

// Satisfy the IDE, which needs to see the include statment in the ino too.
#ifdef dobogusinclude
#include <spi4teensy3.h>
#include <SPI.h>
#endif

USB Usb;
//USBHub Hub1(&Usb); // Some dongles have a hub inside

BTD Btd(&Usb); // You have to create the Bluetooth Dongle instance like so
/* You can create the instance of the class in two ways */
//PS3BT PS3(&Btd); // This will just create the instance


// built in LED on Arduino Mega
int ledPin = 13;                // LED connected to digital pin 13

#if JOHN_MEGA
PS3BT PS3(&Btd, 0x00, 0x11, 0x67, 0xD6, 0xA9, 0x92); // John's Arduino Mega
// 00:11:67:D6:A9:92
// for controlling LANC on the camera
int PinLANC = 49;                // 5V limited input signal from LANC data
int PinCMD = 48;                 // Command to send to LANC
//int PinDEBUG = 22;              // Used for timing and debug as a signal output for a logic analyzer

// for running Bescor MP-101 Pan Tilt Bescor MP-101
int PanRightPin = 12; // OC1B - brown to pin 2 black
int PanLeftPin = 11; // OC1A - green to pin 3 red
int TiltUpPin = 9; // OC2B - blue to pin 5 white
int TiltDownPin = 8; // OC4C - purple to pin 4
int SpeedPin = 7;    //      - black to DB9-1 to DIN 1

int RecLED = 46;
int PS3_LED = 44;
int LANC_LED = 42;
int LANC_HIGH = 30;
int LANC_CHECK = 32;
int LANC_LOW = 34;
#else
PS3BT PS3(&Btd, 0x60, 0x63, 0x77, 0x70, 0xF3, 0x5C);  // Tim's Arduino Mega
// for controlling LANC on the camera
int PinLANC = 3;                // 5V limited input signal from LANC data
int PinCMD = 2;                 // Command to send to LANC
//int PinDEBUG = 22;              // Used for timing and debug as a signal output for a logic analyzer

// for running Bescor MP-101 Pan Tilt Bescor MP-101
int PanLeftPin = 6;  // OC1B - brown to DB9-2 to DIN 2
//int PanRightPin = 7; // OC1A - green to DB9-3 to DIN 6
int PanRightPin = 12; // OC1A - green to DB9-3 to DIN 6
int TiltUpPin = 4;   // OC2B - purple to DB9-4 to DIN 5
int TiltDownPin = 5; // OC4C - blue to DB9-5 to DIN 3
int SpeedPin = 8;    //      - black to DB9-1 to DIN 1

int RecLED = 22;
int PS3_LED = 24;
int LANC_LED = 26;
int LANC_HIGH = 30;
int LANC_CHECK = 32;
int LANC_LOW = 34;
#endif

 
// variables for pan tilt
int panspeed, panspeed_adjusted; // value from RightTopHatX
int tiltspeed, tiltspeed_adjusted; // value from RightTopHatY
int GlobalSpeed = 255;
 
int RecordLEDStatus;

//Timer 1 settings:
//Setting 	Divisor 	Frequency
//0x01 	 	1 	 	31372.55
//0x02 	 	8 	 	3921.16
//0x03  		64 	 	490.20   <--DEFAULT
//0x04  		256 	 	122.55
//0x05 	 	1024 	 	30.64


//Commands to send to camera (note this is not the same value as coded received on status)
const int eSTOP = 0x30;
const int ePLAY = 0x34;
const int eFFWD = 0x38;
const int eREWD = 0x36;
const int eREC  = 0x33;
const int eAutoFocusOnOff = 0x41;
const int eFocusFar       = 0x45;
const int eFocusNear      = 0x47;
const int eAutoExposOnOff = 0x53;
const int eIrisOpen       = 0x55;
const int eIrisClose      = 0x53;
const int eIrisOpen1      = 0x56;
const int eIrisClose1     = 0x54;
const int eIrisAuto       = 0xAF;

const int eZOOMIN  = 0x0E;  // fastest tele, requires command code 0x28
const int eZOOMOUT  = 0x1E; // fastest wide, requires command code 0x28


int eCommand;                   //Current LANC command to send to video camera
int zoomcommandCode;            // zoom based on pot
int nDetectedStatus;            //Status returned by LANC bus
byte nCommandTimes;             //LANC requires at least 5 times to repeat the command to video camera. This will count that.
int nFlashCounter;              //Counter to flash leds so they save battery power (as opposed to be always ON)
bool upButton = false;
bool downButton = false;
bool rightButton = false;
bool leftButton = false;

int speeds[18] = /*These are the commands sent to the camera. 99 tells the code to send nothing.*/
{
  0x0e, 0x0c, 0x0a, 0x08, 0x06, 0x04, 0x02, 0x00, /*Zoom tele, from fastest to slowest*/
  ZOOM_DEAD_SPACE, ZOOM_DEAD_SPACE, ZOOM_DEAD_SPACE, /*Dead zone*/
  0x10, 0x12, 0x14, 0x16, 0x18, 0x1a, 0x1e        /*Zoom wide, from slowest to fastest*/
}; 

int Limit(int low, int high, int value) {
  if (value < low)
    return low;
  if (value > high)
    return high;
    
  return value;
}


void FlashLED()
{
  switch (nDetectedStatus)
  {
    case LANC_STAT_PAUSE: //pause
      if (PS3.PS3Connected) {
        if (RecordLEDStatus != 1) {
          RecordLEDStatus = 1;
          digitalWrite(RecLED, LOW);   // sets the LED off
          PS3.setLedOff();
          PS3.setLedOn(LED4);}
      }
      break;
    case LANC_STAT_PLAY: //PLAY
      break;
    case LANC_STAT_REC: //REC
      if (PS3.PS3Connected) {
        if (RecordLEDStatus != 2) {
          RecordLEDStatus = 2;
          PS3.setLedOff();
          PS3.setLedOn(LED3);
          digitalWrite(RecLED, HIGH);   // sets the LED on
        }
      }
      break;
    default: //any other activity except STOP
      break;
  }
}

void BlinkLong()
{
   digitalWrite(LANC_LED, HIGH);   // sets the LED on
   delay(LED_BLINK_DELAY);                  // waits for a second
   digitalWrite(PS3_LED, HIGH);   // sets the LED on
   delay(LED_BLINK_DELAY);                  // waits for a second
   digitalWrite(RecLED, HIGH);   // sets the LED on
   delay(LED_BLINK_DELAY);                  // waits for a second
   digitalWrite(RecLED, LOW);    // sets the LED off
   delay(LED_BLINK_DELAY);                  // waits for a second
   digitalWrite(PS3_LED, LOW);    // sets the LED off
   delay(LED_BLINK_DELAY);                  // waits for a second
   digitalWrite(LANC_LED, LOW);    // sets the LED off
   delay(LED_BLINK_DELAY);                  // waits for a second
}

void AfterLongGap()
{
  boolean bLongEnough = false;
  int nInd;

  //search for 4ms gap -at least- being high continuosly.
  //This worked extraordinarily well in my European Sony video camera
  //but it may not work on a NTSC camera, you may need to reduce the
  //loop limit for that (hopefully not). Please send me details
  //in that case to make it available to the community.

  while (!bLongEnough)
  {
    for (nInd = 0; nInd < LANC_GAP_LIMIT; nInd++)
    {
      delayMicroseconds(SEARCH_GAP_DELAY);
      if (digitalRead(PinLANC) == LOW)
        break;
    }
    if (nInd == LANC_GAP_LIMIT)
      bLongEnough = true;
  }

  //Now wait till we get the first start bit (low)
  while (digitalRead(PinLANC) == HIGH)
    delayMicroseconds(SEARCH_GAP_DELAY);

}

void SendCommand(unsigned char nCommand)
{
  //Note bits are inverted already by the NPN on open collector
  //(when it is switched ON by a vbe>0.7v, the vce will be close to zero thus low state for LANC)
  //This is exactly what we want and how LANC bus works (tie to GND to indicate "1")

  //Note timing here is critical, I used a logic analyzer to rev engineering right delay times.
  //This guarantees closer to LANC standard so it increases chances to work with any videocamera,
  //but you may need to finetune these numbers for yours (hopefully not). Please send me details
  //in that case to make it available to the community.

  delayMicroseconds(LANC_START_BIT_DELAY); //ignore start bit
  digitalWrite(PinCMD, nCommand  & 0x01);

  for (int i = 1; i < LANC_BYTE; ++i) {
     delayMicroseconds(LANC_BIT_DELAY);
     digitalWrite(PinCMD, (nCommand  & (1 << i)) >> i);
  }

  delayMicroseconds(LANC_BIT_DELAY);
  digitalWrite(PinCMD, LOW); //free the LANC bus after writting the whole command
}

byte GetNextByte()
{
  unsigned char nByte = 0;

  //Timming here is not as critical as when writing, actually it is a bit delayed
  //when compared to start edge of the signal, but this is good as it increases
  //chances to read in the center of the bit status, thus it is more immune to noise
  //Using this I didn't need any hardware filter such as RC bass filter.

  //ignore start bit, delay comes before the first read
  
  for (int i = 0; i < LANC_BYTE; ++i) {
     delayMicroseconds(LANC_READ_BIT_DELAY);
     nByte |= digitalRead(PinLANC) << i;
  }

  nByte = nByte ^ BYTE_MASK;  //invert bits, we got LANC LOWs for logic HIGHs

  return nByte;
}

void NextStartBit()
{
  //Now wait till we get the first start bit (low)
  while (1)
  {
    //this will look for the first LOW signal with abou 5uS precission
    while (digitalRead(PinLANC) == HIGH)
      delayMicroseconds(LANC_NEXT_BIT_DELAY);

    //And this guarantees it was actually a LOW, to ignore glitches and noise
    delayMicroseconds(LANC_NEXT_BIT_DELAY);
    if (digitalRead(PinLANC) == LOW)
      break;
  }
}

void PrintAddress(String start = "") {
  Serial.print("Bluetooth Dongle Bluetooth Address: ");
  for (int i = 0; i < 6; ++i) {
    Serial.print(start);
    Serial.print(" 0x");
    Serial.print(Btd.my_bdaddr[i], HEX);
  }
}

void GetCommand()
{
  if (PS3.PS3Connected) { // The Navigation controller only have one joystick

    if (PS3.getButtonClick(SELECT)) {
      Serial.print(F("\r\nSelect - "));
      PrintAddress();
      PS3.printStatusString();
    }

    if (PS3.getButtonPress(UP)) {
      upButton = true;
      GlobalSpeed = SPEED_CRAWL;
      #if !JOHN_MEGA
      analogWrite(SpeedPin, GlobalSpeed);
      #endif
      analogWrite(TiltDownPin, 0);
      analogWrite(TiltUpPin, TILT_UP_CRAWL);
      Serial.print("\r\nCrawling Up");
    }
    else if (PS3.getButtonPress(DOWN)) {
      downButton = true;
      GlobalSpeed = SPEED_CRAWL;
      #if !JOHN_MEGA
      analogWrite(SpeedPin, GlobalSpeed);
      #endif
      analogWrite(TiltUpPin, 0);
      analogWrite(TiltDownPin, TILT_DOWN_CRAWL);
      Serial.print("\r\nCrawling Down");
    }
    if (PS3.getButtonPress(LEFT)) {
      leftButton = true;
      GlobalSpeed = LEFT_SPEED_CRAWL;
      #if !JOHN_MEGA
      analogWrite(SpeedPin, GlobalSpeed);
      #endif
      analogWrite(PanRightPin, 0);
      analogWrite(PanLeftPin, PAN_LEFT_CRAWL);
      Serial.print("\r\nCrawling Left");
    }
    else if (PS3.getButtonPress(RIGHT)) {
      rightButton = true;
      GlobalSpeed = SPEED_CRAWL;
      #if !JOHN_MEGA
      analogWrite(SpeedPin, GlobalSpeed);
      #endif
      analogWrite(PanLeftPin, 0);
      analogWrite(PanRightPin, PAN_RIGHT_CRAWL);
      Serial.print("\r\nCrawling Right");
    }

    zoomcommandCode = speeds[map(PS3.getAnalogHat(LeftHatY), 0, BYTE_MASK, 0, ZOOM_SPEED_NUM_OPS)]; //The PS3 LeftHatY returns from 0 to 255, but our array is from 0 to 17. This converts the values.

    if (zoomcommandCode == ZOOM_DEAD_SPACE) {
      if (PS3.getButtonClick(START)) {
        Serial.print("\r\nREC Button Pressed.");
        eCommand = eREC;
        nCommandTimes = 0;
      }
      else if (PS3.getButtonClick(L1)) {
        eCommand = eIrisClose;
        nCommandTimes = 0;
      }
      else if (PS3.getButtonClick(R1)) {
        eCommand = eIrisOpen;
        nCommandTimes = 0;
      }
      else if (PS3.getAnalogButton(L2) > L2R2_MIN) {
        eCommand = eIrisClose1;
        nCommandTimes = 0;
      }
      else if (PS3.getAnalogButton(R2) > L2R2_MIN) {
        eCommand = eIrisOpen1;
        nCommandTimes = 0;
      }
#if MANUAL_FOCUS_CONTROLS
      else if (PS3.getButtonPress(TRIANGLE)) {
        eCommand = eFocusFar;
        nCommandTimes = 0;
      }
      else if (PS3.getButtonPress(CROSS)) {
        eCommand = eFocusNear;
        nCommandTimes = 0;
      }
#endif
      else if (PS3.getButtonClick(SQUARE)) {
        eCommand = eAutoFocusOnOff;
        nCommandTimes = 0;
      }
      else if (PS3.getButtonClick(CIRCLE)) {
        eCommand = eIrisAuto;
        nCommandTimes = 0;
      }
    }
    else {
      eCommand = zoomcommandCode;
      nCommandTimes = 0;
    }
  }
  else // not connected
  {
    zoomcommandCode = ZOOM_DEAD_SPACE;
//    digitalWrite(PinDEBUG, HIGH); // turn OFF connected LED
    RecordLEDStatus = 0; // Re-establish state next connection
  }
}




//---------------------------MAIN---------------------------------

void setup()                    // run once, when the sketch starts
{
  Serial.begin(BAUD_RATE);
#if !defined(__MIPSEL__)
  while (!Serial); // Wait for serial port to connect - used on Leonardo, Teensy and other boards with built-in USB CDC serial connection
#endif
  if (Usb.Init() < 0) {
    Serial.print(F("\r\nOSC did not start"));
    while (1); //halt
  }
  Serial.println(F("\r\nPS3 Bluetooth Library Started"));

  //set input pins and activate internal weak pullup resistors
  pinMode(ledPin, OUTPUT);
  pinMode(RecLED, OUTPUT);
  pinMode(PS3_LED, OUTPUT);
  pinMode(LANC_LED, OUTPUT);

  pinMode(PanLeftPin, OUTPUT);
  pinMode(TiltUpPin, OUTPUT);
  pinMode(TiltUpPin, OUTPUT);
  pinMode(TiltDownPin, OUTPUT);
  #if !JOHN_MEGA
  pinMode(SpeedPin, OUTPUT);
  #endif

  pinMode(PinLANC, INPUT);
  pinMode(PinCMD, OUTPUT);
 // pinMode(LANC_HIGH, OUTPUT);
  pinMode(LANC_CHECK, INPUT_PULLUP);
  pinMode(LANC_LOW, OUTPUT);
  //Must connect a wire from pin LANC_HIGH to pin LANC_CHECK to enable LANC while running
  //or connect a wire from pin LANC_LOW to pin LANC_CHECK to disable LANC whlie running
  digitalWrite(LANC_HIGH, HIGH);
  digitalWrite(LANC_LOW, LOW);

  nFlashCounter = 0;
  nCommandTimes = LANC_NUM_CMD;

  TCCR1B = PWM_SPEED; // setup PWM speed of pins 11 and 12 on Timer 1, 0x03 = default
  TCCR2B = PWM_FREQ; //Adjusting PWM frequencies for testing pins 11 and 3

  RecordLEDStatus = 0;
 
}

void loop()                     // run over and over again
{
  byte LANC_Frame[LANC_BYTE];
  byte nByte;
  int nInd, nInd0;
  
  bool fast_tilting = false;
 // bool slow_panning = true;

  BlinkLong();

  eCommand = eSTOP;

  //infinite loop, I do not want to get out of here ever.
  //It is more efficient than let loop() function to do
  //that, as it requires a function call and then a loop
  //like this one. Well, I prefer my own efficient loop
  //instead. This increase chances of AfterLongGap() having
  //time enough for what it needs to be done.


  while (1)
  {
    Usb.Task();  
    
    if (PS3.PS3Connected) { // The Navigation controller only have one joystick
      digitalWrite(PS3_LED, HIGH);   // sets the LED on
    
      if (PS3.getButtonClick(PS)) {
        PS3.disconnect();
        digitalWrite(PS3_LED, LOW);   // sets the LED off
      }
      
      panspeed = PS3.getAnalogHat(RightHatX);
      //In case you need to adjust movements based on the current panning action
     /* if (panspeed > SLOW_PAN_RIGHT && panspeed <= FAST_POSITIVE ||
          panspeed >= FAST_NEGATIVE && panspeed < SLOW_PAN_LEFT && ) { //Slow Right or Left Pan
        slow_panning = true;
        if (panspeed > JOSTICK_MIDDLE)
          panspeed_adjusted = map(panspeed, SLOW_POSITIVE, FAST_POSITIVE, SLOW_PAN_R_MAP, FAST_PAN_R_MAP);
        else
          panspeed_adjusted = map(panspeed, SLOW_PAN_LEFT, FAST_NEGATIVE, SLOW_PAN_L_MAP, FAST_PAN_L_MAP);
      } */
      //---------------------TILTING-------------------------------------------
      tiltspeed = PS3.getAnalogHat(RightHatY);
      if (tiltspeed > SLOW_POSITIVE)
      {
        if (tiltspeed > FAST_POSITIVE)
          fast_tilting = true;
      
        tiltspeed_adjusted = map(tiltspeed, JOSTICK_MIDDLE, BYTE_MASK, 0, BYTE_MASK);
        GlobalSpeed = Limit(MIN_SPEED, MAX_SPEED, tiltspeed_adjusted);
        #if !JOHN_MEGA
        analogWrite(SpeedPin, GlobalSpeed);
        #endif
        Serial.print("\n\rTilt Down Speed: ");
        Serial.print(tiltspeed_adjusted);
        Serial.print("   SpeedPin: ");
        Serial.print(GlobalSpeed);
        analogWrite(TiltDownPin, tiltspeed_adjusted);
        analogWrite(TiltUpPin, 0);
      }
      else if (tiltspeed < SLOW_TILT_UP)
      {
        if (tiltspeed < FAST_NEGATIVE)
          fast_tilting = true;
          
        tiltspeed_adjusted = map(tiltspeed, JOSTICK_MIDDLE, 0, 0, BYTE_MASK);
        GlobalSpeed = Limit(MIN_SPEED - UP_DOWN_DISPARITY, MAX_SPEED, map(tiltspeed, JOSTICK_MIDDLE, 0, 0, BYTE_MASK));
        #if !JOHN_MEGA
        analogWrite(SpeedPin, GlobalSpeed);
        #endif
        Serial.print("\n\rTilt Up Speed: ");
        Serial.print(tiltspeed_adjusted);
        Serial.print("   SpeedPin: ");
        Serial.print(GlobalSpeed);
        analogWrite(TiltDownPin, 0);
        analogWrite(TiltUpPin, tiltspeed_adjusted);
      }
      else
      {
        tiltspeed_adjusted = 0;
        analogWrite(TiltUpPin, 0);
        analogWrite(TiltDownPin, 0);
      }
      
      //---------------------PANNING-------------------------------------------    
      if (panspeed > FAST_POSITIVE) //Fast Right Pan
      {
        panspeed_adjusted = map(panspeed, FAST_POSITIVE, BYTE_MASK, FAST_PAN_R_MAP, BYTE_MASK);
        GlobalSpeed = Limit(MIN_SPEED, MAX_SPEED, panspeed_adjusted);
        #if !JOHN_MEGA
        analogWrite(SpeedPin, GlobalSpeed);
        #endif
        Serial.print("\n\rPan Right Speed: ");
        Serial.print(panspeed_adjusted);
        Serial.print("   SpeedPin: ");
        Serial.print(GlobalSpeed);
        analogWrite(PanRightPin, panspeed_adjusted);
        analogWrite(PanLeftPin, 0);
      }
      else if (panspeed > SLOW_PAN_RIGHT) //Slower Right Pan
      {
        panspeed_adjusted = map(panspeed, SLOW_POSITIVE, FAST_POSITIVE, SLOW_PAN_R_MAP, FAST_PAN_R_MAP);
        if (!fast_tilting) {
          GlobalSpeed = Limit(MIN_SPEED, MAX_SPEED, panspeed_adjusted);
          #if !JOHN_MEGA
          analogWrite(SpeedPin, GlobalSpeed);
          #endif
        }
        Serial.print("\n\rPan Right Speed: ");
        Serial.print(panspeed_adjusted);
        Serial.print("   SpeedPin: ");
        Serial.print(GlobalSpeed);
        analogWrite(PanRightPin, panspeed_adjusted);
        analogWrite(PanLeftPin, 0);
      }
      else if (panspeed < FAST_NEGATIVE) //Fast Left Pan
      {
        panspeed_adjusted = map(panspeed, FAST_NEGATIVE, 0, FAST_PAN_L_MAP, BYTE_MASK);
        GlobalSpeed = Limit(MIN_SPEED, MAX_SPEED, panspeed_adjusted);
        #if !JOHN_MEGA
        analogWrite(SpeedPin, GlobalSpeed);
        #endif
        Serial.print("   SpeedPin: ");
        Serial.print(GlobalSpeed);
        Serial.print("\n\rPan Left Speed: ");
        Serial.print(panspeed_adjusted);
        analogWrite(PanRightPin, 0);
        analogWrite(PanLeftPin, panspeed_adjusted);
      }
      else if (panspeed < SLOW_PAN_LEFT) //Slower Left Pan
      {
        panspeed_adjusted = map(panspeed, SLOW_PAN_LEFT, FAST_NEGATIVE, SLOW_PAN_L_MAP, FAST_PAN_L_MAP);
        if (!fast_tilting) {
          GlobalSpeed = Limit(MIN_SPEED, MAX_SPEED, panspeed_adjusted);
          #if !JOHN_MEGA
          analogWrite(SpeedPin, GlobalSpeed);
          #endif
        }
        Serial.print("   SpeedPin: ");
        Serial.print(GlobalSpeed);
        Serial.print("\n\rPan Left Speed: ");
        Serial.print(panspeed_adjusted);
        analogWrite(PanRightPin, 0);
        analogWrite(PanLeftPin, panspeed_adjusted);
      }
      else
      {
        panspeed_adjusted = 0;
        analogWrite(PanRightPin, 0);
        analogWrite(PanLeftPin, 0);
      }
    }

    fast_tilting = false;
   // slow_panning = false;
    upButton = false;
    downButton = false;
    rightButton = false;
    leftButton = false;
     
    //Which command do I have to send from my push buttons?
    GetCommand();

    //-------------------------LANC----------------------------------------
    //Synchronize to get start of next frame
#if LANC_ENABLE
    if (digitalRead(LANC_CHECK) == HIGH) {
      AfterLongGap();
  
      digitalWrite(LANC_LED, HIGH);   // sets the LED on
  
      //I will send the command 8 now 5 times, LANC requires at least 5 times
      if (nCommandTimes < LANC_NUM_CMD - 1)
      {
        nCommandTimes++;
  
        if (eCommand == eREC) {
          SendCommand(LANC_NORMAL_CMD); //0x18 indicates normal command to videocamera.
        }
        else
        {
          SendCommand(LANC_SPECIAL_CMD); //0x28 indicates special command to videocamera.
        }
  
        //SendCommand(LANC_SPECIAL_CMD); //0x28 indicates special command to videocamera.
        //you have to change this for a photo camera.
        
        NextStartBit();
        SendCommand(eCommand);
      }
      else
      {
        eCommand = eSTOP;
        //Command already sent 8 times so, just ignore next 2 bytes
        GetNextByte();
        NextStartBit();
        GetNextByte();
      }
  
      //Get next 6 bytes remaining
      for (nInd = LANC_DONE_BYTES; nInd < LANC_BYTE; nInd++)
      {
        NextStartBit();
        LANC_Frame[nInd] = GetNextByte();
      }
  
      nDetectedStatus = LANC_Frame[LANC_REPORT];
    }
    else {     //LANC disabled by toggle switch
      digitalWrite(LANC_LED, LOW);   // sets the LED on
    }
#endif
    FlashLED();



  }

  Serial.println("****Unexpected Loop here, if you reach here it is an ERROR!");
}

/*

 HEX values rev engineered from my Sony video camera in byte #4.
 You can get yours by simply activating the RS232 dump and manually
 playing with your camera, the codes you see in byte #4 will let you
 check for a particular value programmatically.

 2 -stop
 4 - rec
 6 -play
 46 -ffwd with play
 56 -rew with play
 7 -play pause
 83 -rew
 3 -ffwd

 */
