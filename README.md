# PS3 Camera Control Library

This repository details the implementation of PS3-controlled camera rig with control over LANC commands to the camera as well as a motorized Pan/Tilt head for control camera orientation.

![CameraControlWiringDiagram](PS3CameraControl/Diagrams/CameraControlWiringDiagram.png)

## Parts List ##

The following parts are used to make use of the code contained in this repository.

| Part                           | Necessity  | Description                                                     |
| ------------------------------ |:----------:|:--------------------------------------------------------------- |
| Arduino Mega 2560 *            | Required   | [Arduino Mega 2560](https://www.arduino.cc/en/Guide/ArduinoMega2560) |
| Bescor MP-101 Pan-Tilt Head    | Required   | [Bescor MP-101](https://bescor.com/product-category/motorized-pan-head/) |
| LANC-compatible Camcorder      | Required   | [General Identification](http://www.appliedlogiceng.com/index_files/Page1049.htm) ------  [Wikipedia](https://en.wikipedia.org/wiki/LANC ) |
| DIN-7 Male Connector           | Required   | Circular DIN-7 connector - Male - Not Mini-DIN! ------  [Wikipedia](https://en.wikipedia.org/wiki/DIN_connector) |
| LANC Male Cable                | Required   | 2.5mm tip-ring-sleeve jack ------ [Amazon](https://www.amazon.com/dp/B01IVJ1DE8/) ------ [Amazon Search](https://www.amazon.com/s/field-keywords=male+male+lanc+cable) |
| DB9 Female Connector           | Required   | [9-pin Serial connector](https://en.wikipedia.org/wiki/D-subminiature) |
| DB9 Male Connector             | Required   | [9-pin Serial connector](https://en.wikipedia.org/wiki/D-subminiature) |
| 2N7000 Transistor (or similar) | Required   | To Pull LANC data line low ------ [2N7000](http://www.mouser.com/ProductDetail/ON-Semiconductor-Fairchild/2N7000/?qs=sGAEpiMZZMshyDBzk1%2FWi9bHELEahoDnY1fyKF6A6Ko%3D) |
| SPDT Toggle Switch             | Optional   | Can use a wire from LANC\_LOW to LANC\_CHECK pins (defined in code) to disable LANC while running.** |
| Indicator LEDs                 | Optional   | Red - Record --- Yellow - PS3 Connected --- Green - LANC Detected |
| LED Resistors                  | Optional   | 200-500 ohm to current limit the LEDs |
*Must be a MEGA, not an UNO, we need 5 PWM pins and only 3 are available on the UNO because the USB Sheild is using the rest of the PWM pins <br>
**A toggle switch is more conveinient if you want to be able to switch on and off the LANC functionality while running, but if you never want LANC functionality, you can just use a wire.
Without a wire or toggle switch attached, the LANC functionality is enabled, because the PULLUP resistor on the LANC\_CHECK pin is enabled. <br>
Note: You can turn off LANC functionality while running, but you cannot disconnect the LANC camcorder before disabling the LANC functionality, or the program will wait forever for the LANC data that is no longer available.
The disable must happen before the disconnect. You may also power cycle the arduino (or reset) after disabling the LANC functionailty if it does get stuck waiting for a disconnected LANC line.

## Controls ##
| Control                 | Description                                                     |
| ----------------------- |:--------------------------------------------------------------- |
| PS Button               | Connect over Bluetooth                                          |
| Right Stick Up/Down     | Tilt Up/Down with speed control                                 |
| Right Stick Left/Right  | Pan Left/Right with speed control                               |
| Left Stick Up/Down      | Zoom In/Out with speed control                                  |
| Up/Down                 | Tilt Up/Down crawling speed                                     |
| Left/Right              | Pan Left/Right crawling speed                                   |
| Start                   | Record / Pause                                                  |
| Select                  | Print Status on UART                                            |
| Square                  | Auto Focus On/Off                                               |
| Triangle                | Focus Farther                                                   |
| Cross                   | Auto Nearer                                                     |
| Circle                  | Auto Iris (not working for all cameras)                         |
| R1                      | Iris More Open (not working for all cameras)                    |
| L1                      | Iris More Closed (not working for all cameras)                  |


## Continuity List ##

| Description                  | Wire Color | DB9 Pin       | DIN/LANC Pin | Mega2560 Pin | Transistor Pin|
| ---------------------------- |:----------:|:-------------:|:------------:|:------------:|:-------------:|
| Bescor Speed                 | Black      | DB9-1         | DIN7-1       | MEGA Pin 8   | N/A           |
| Pan Left                     | Brown      | DB9-2         | DIN7-2       | MEGA Pin 7   | N/A           |
| Pan Right                    | Green      | DB9-3         | DIN7-6       | MEGA Pin 6   | N/A           |
| Tilt Up                      | Purple     | DB9-4         | DIN7-5       | MEGA Pin 5   | N/A           |
| Tilt Down                    | Blue       | DB9-5         | DIN7-3       | MEGA Pin 4   | N/A           |
| LANC Ground                  | Yellow     | DB9-6         | LANC Sleeve  | MEGA Pin GND | 2N7000 Source |
| LANC Data (from camcorder)   | Orange     | DB9-7         | LANC Tip     | MEGA Pin 3   | 2N7000 Drain  |
| LANC Command (to transistor) | Red        | N/A           | LANC Tip     | MEGA Pin 2   | 2N7000 Gate   |
| LANC Ring - Not Used         | Teal       | DB9-8         | DIN7-7       | N/A          | N/A           |
| Bescor Ground - Not Used     | White      | DB9-9         | LANC Ring    | N/A          | N/A           |
